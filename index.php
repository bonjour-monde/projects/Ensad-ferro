<?php

    // echo "here we nned to build a form to send a svg font and process it then remove everything escept the broken one"
    require_once './function.php';
 ?>
 <?php
 session_start();
    if (isset($_POST['submit'])) {
        if (isset($_FILES['uploadedFile']) && $_FILES['uploadedFile']['error'] === UPLOAD_ERR_OK) {
            $fileTmpPath = $_FILES['uploadedFile']['tmp_name'];
            $fileName = $_FILES['uploadedFile']['name'];
            $fileSize = $_FILES['uploadedFile']['size'];
            $fileType = $_FILES['uploadedFile']['type'];


            $fileNameCmps = explode(".", $fileName);
            $fileExtension = strtolower(end($fileNameCmps));
            //sanitize file name
            $newFileName = remove_accents($fileName);

            $grid = $_POST['grid'];

            $allowedfileExtensions = array('ttf','otf');
            if (in_array($fileExtension, $allowedfileExtensions)) {
                    $uploadFileDir = './import/';
                    $dest_path = $uploadFileDir . $newFileName;
                    $name = explode(".", $newFileName);
                    if(move_uploaded_file($fileTmpPath, $dest_path))
                    {
                      //$message ='File is successfully uploaded.';
                      $fullpath = "./magnetfont/magnetface.py";
                      // $output = shell_exec('./magnetfont/convert2svgfont.pe ./import/'.$newFileName);
                      if(int($grid) < 12){
                          $output = shell_exec('./magnetfont/convert2svgfont.pe ./import/'.$newFileName.' & ./venvmagnetface/bin/python3 '.$fullpath.' ./import/'.$name[0].'.svg --grid '.$grid);
                          echo $output;
                          $output  = shell_exec( './magnetfont/script.pe ./export/'.$name[0].'-'.$grid.'.svg');
                          echo '<a tagret="_blank" id="exportok" href="export"> Find your font here ! </a>';
                          unlink('./export/'.$name[0].'-'.$grid.'.svg');
                      }
                    }
                    else
                    {
                      $message = 'error';
                    }
            }
        }
    }
 ?>
 <!DOCTYPE html>
 <html>
 <head>
     <meta http-equiv="Expires" content="Tue, 01 Jan 1995 12:12:12 GMT">
     <meta http-equiv="Pragma" content="no-cache">
   <title>FerroForge</title>
   <style media="screen">
   @font-face {
    font-family: "ferroforge1";
    font-style: normal;
    src: url("fonts/FerroForge-Romain.otf");
   }
   @font-face {
    font-family: "ferroforge5";
    font-style: normal;
    src: url("fonts/FerroForge-Romain-6.otf");
   }
   @font-face {
    font-family: "ferroforge4";
    font-style: normal;
    src: url("fonts/FerroForge-Romain-8.otf");
   }
   @font-face {
    font-family: "ferroforge3";
    font-style: normal;
    src: url("fonts/FerroForge-Romain-10.otf");
   }
   @font-face {
    font-family: "ferroforge2";
    font-style: normal;
    src: url("fonts/FerroForge-Romain-12.otf");
   }

     body{
       background: rgb(83, 34, 245);
       text-align: center;
       color:#eeeeee;
     }
     h1{
       font-family: "ferroforge2";
       top:1em;
       display: inline-block;
       height: 1em;
       font-size: 15em;
     }
     h1:hover{
       font-family: "ferroforge2";
     }
     canvas{
       position: absolute;
       top:0;
       left:0;
       z-index: -666;
     }
     #menu{
       font-family: "ferroforge1";
       font-size: 1.75em;
     }
     #fileupload{
       font-family: "ferroforge1";
       font-size: .7em;
     }
     .rightBottom {
       padding: 1em;
    transition: margin-top 0.3s ease,
    margin-left 0.3s ease,
    box-shadow 0.3s ease;

    background:rgb(83, 34, 245);
    border: solid 2.5px #eeeeee;

    box-shadow: 1px 0px 0px #eeeeee,0px 1px 0px #eeeeee,
    2px 1px 0px #eeeeee,1px 2px 0px #eeeeee,
    3px 2px 0px #eeeeee,2px 3px 0px #eeeeee,
    4px 3px 0px #eeeeee,3px 4px 0px #eeeeee,
    5px 4px 0px #eeeeee,4px 5px 0px #eeeeee,
    6px 5px 0px #eeeeee,5px 6px 0px #eeeeee,
    7px 6px 0px #eeeeee,6px 7px 0px #eeeeee,
    8px 7px 0px #eeeeee,7px 8px 0px #eeeeee,
    9px 8px 0px #eeeeee,8px 9px 0px #eeeeee;
 }
 .leftBottom {
  padding: 1em;
 transition: margin-top 0.3s ease,
 margin-left 0.3s ease,
 box-shadow 0.3s ease;

 background:rgb(83, 34, 245);
 border: solid 2.5px #eeeeee;

 box-shadow:-1px 0px 0px #eeeeee,-0px 1px 0px #eeeeee,
 -2px 1px 0px #eeeeee,-1px 2px 0px #eeeeee,
 -3px 2px 0px #eeeeee,-2px 3px 0px #eeeeee,
 -4px 3px 0px #eeeeee,-3px 4px 0px #eeeeee,
 -5px 4px 0px #eeeeee,-4px 5px 0px #eeeeee,
 -6px 5px 0px #eeeeee,-5px 6px 0px #eeeeee,
 -7px 6px 0px #eeeeee,-6px 7px 0px #eeeeee,
 -8px 7px 0px #eeeeee,-7px 8px 0px #eeeeee,
 -9px 8px 0px #eeeeee,-8px 9px 0px #eeeeee;
 }
     input[type="submit"],input[type="file"]{
       font-family: "ferroforge1";
       font-size: 1.75em;
       margin-top: 1.5em;
       background: none;
       background: rgb(83, 34, 245);
     }
     input[type="submit"], #fileupload + label{
       color:#eeeeee;
       transition: .3s background, .3s color;
     }
     input[type="submit"]:hover{
       background:#eeeeee;
       color:#222222;
       cursor: pointer;

     }
    input[type="submit"]{
    margin-left:4em;
 }
     input[type="text"]{
       font-family: "ferroforge1";
       font-size: 1.5em;
       background: none;
       border: 0;
       color:#eeeeee;
       width: 2cm;
     }
     form{
       margin-top: -5em;
     }
     #grid{
       -webkit-appearance: none;
 height: 20px;
 background: #eeeeee;
 outline: none;
 -webkit-transition: .2s;
 transition: opacity .2s;
 margin-left:7em;
 margin-bottom: -.5em;
     }
     #grid::-webkit-slider-thumb {
  -webkit-appearance: none;
  appearance: none;
  width: 25px;
  height: 25px;
  border-radius: 50%;
  background: rgb(83, 34, 245);
  border: 2px solid #eeeeee;
  cursor: pointer;
  transition: .2s border-radius;
 }

 #grid::-moz-range-thumb {
  border: 2px solid #eeeeee;
  width: 25px;
  height: 25px;
  border-radius: 50%;
  background: rgb(83, 34, 245);
  cursor: pointer;
  transition: .2s border-radius;
 }

 #grid::-webkit-slider-thumb:hover {
 border-radius: 5%;
 }

 #grid::-moz-range-thumb:hover {
 border-radius: 5%;
 }

 #fileupload {
  width: 0.1px;
  height: 0.1px;
  opacity: 0;
  overflow: hidden;
  position: absolute;
  z-index: -1;
 }
 #fileupload + label {
    font-size: 1em;
    color: white;
    display: inline-block;
    margin-right: 3em;
 }

 .taillebouton{
  width:5cm;
  height: 2cm;
 }

 #fileupload:focus + label,
 #fileupload + label:hover {
  background:#eeeeee;
  color:#222222;
    cursor: pointer;
 }
 #exportok{
  position: fixed;
  background: #222222;
  top:50vh;
  left:10vw;
  width: 80vw;
  font-size: 50pt;
  font-family: "ferroforge3";
  z-index: 9999;
 }
 #exportok a{
  font-family: "ferroforge5";
  color:yellow;
  text-decoration: none;
  transition: .3s color;
 }
 #exportok a:hover{
  color:white;
 }
 #txtgrid{
  position: absolute;
  width:13em;
  text-align: center;
  background: rgb(83, 34, 245);
  color:#eeeeee;
  border: 2px solid #eeeeee;
  padding: .1em .1em .2em .1em;
  margin-top: .4em;
 }
 ::selection {
  background-color: #eeeeee;
  color:rgb(83, 34, 245);
}
   </style>
 </head>
 <body onload="updateTextInput(3);">
   <canvas id="particlesField">This Browser is not supported</canvas>

   <h1 id="title"> FerroForge </h1>
   <?php
     if (isset($_SESSION['message']) && $_SESSION['message'])
     {
       printf('<b>%s</b>', $_SESSION['message']);
       unset($_SESSION['message']);
     }
   ?>
   <form method="POST" action="index.php" enctype="multipart/form-data">
     <span id="menu">
       <input type="file" name="uploadedFile" id="fileupload" class="inputfile" data-multiple-caption="{count} files selected" multiple />
       <label for="fileupload" class="rightBottom"><span>Upload an OTF file</span></label>
       <span id="txtgrid">Grid definition</span>
       <input type="range" id="grid" name="grid" min="1" max="10" value="3" onchange="updateTextInput(this.value);">
      <input type="text" id="textInput" value="">
    </span>

     <input type="submit" name="submit" value="Generate my FerroFont" class="leftBottom"/>

   </form>
   <script type="text/javascript">

       function updateTextInput(val) {
          document.getElementById('textInput').value=val;
        }
        var y = setTimeout(function() {
          document.getElementById("title").style.fontFamily = "ferroforge3";
        }, 1000);
        var z = setTimeout(function() {
          document.getElementById("title").style.fontFamily = "ferroforge4";
        }, 2000);
        var a = setTimeout(function() {
          document.getElementById("title").style.fontFamily = "ferroforge5";
        }, 3000);
        var b = setTimeout(function() {
          document.getElementById("title").style.fontFamily = "ferroforge4";
        }, 4000);
        var b = setTimeout(function() {
          document.getElementById("title").style.fontFamily = "ferroforge3";
        }, 5000);
        var b = setTimeout(function() {
          document.getElementById("title").style.fontFamily = "ferroforge2";
        }, 6000);

      var x = setInterval(function() {
        var y = setTimeout(function() {
          document.getElementById("title").style.fontFamily = "ferroforge3";
        }, 1000);
        var z = setTimeout(function() {
          document.getElementById("title").style.fontFamily = "ferroforge4";
        }, 2000);
        var a = setTimeout(function() {
          document.getElementById("title").style.fontFamily = "ferroforge5";
        }, 3000);
        var b = setTimeout(function() {
          document.getElementById("title").style.fontFamily = "ferroforge4";
        }, 4000);
        var b = setTimeout(function() {
          document.getElementById("title").style.fontFamily = "ferroforge3";
        }, 5000);
        var b = setTimeout(function() {
          document.getElementById("title").style.fontFamily = "ferroforge2";
        }, 6000);
      }, 6000);



      (function e(t, n, r) {
  function s(o, u) {
    if (!n[o]) {
      if (!t[o]) {
        var a = typeof require == "function" && require;
        if (!u && a) return a(o, !0);
        if (i) return i(o, !0);
        var f = new Error("Cannot find module '" + o + "'");
        throw f.code = "MODULE_NOT_FOUND", f
      }
      var l = n[o] = {
        exports: {}
      };
      t[o][0].call(l.exports, function(e) {
        var n = t[o][1][e];
        return s(n ? n : e)
      }, l, l.exports, e, t, n, r)
    }
    return n[o].exports
  }
  var i = typeof require == "function" && require;
  for (var o = 0; o < r.length; o++) s(r[o]);
  return s
})({
  "./src/js/index.js": [function(require, module, exports) {
    var Particle = require('./particles'),
      canvas = document.querySelector('#particlesField'),
      ctx = canvas.getContext('2d'),
      width = window.innerWidth,
      height = window.innerHeight,

      presetDefault = {
        count: 50,
        size: 1,
        minSpeed: 2,
        maxSpeed: 50,
        startOrigin: {
          x: undefined,
          y: undefined
        }
      },

      presetFast = {
        count: 100,
        size: 1,
        minSpeed: 20,
        maxSpeed: 100,
        startOrigin: {
          x: undefined,
          y: undefined
        }
      },

      presetCentralExplode = {
        count: 1000,
        size: 1,
        minSpeed: 1,
        maxSpeed: 100,
        startOrigin: {
          x: width / 2,
          y: height / 2
        }
      },

      presetInsaneRandomSizeFromLeftTop = {
        count: 2000,
        size: function() {
          return Math.random() * 10 + 1;
        },
        minSpeed: 20,
        maxSpeed: 100,
        startOrigin: {
          x: 1,
          y: 1
        }
      },

      presetInsaneRandomSizeFromCenter = {
        count: 2000,
        size: function() {
          return Math.random() * 2 + 0.2;
        },
        minSpeed: 20,
        maxSpeed: 100,
        startOrigin: {
          x: width / 2,
          y: height / 2
        }
      },

      settings = presetDefault;

    window.generateParticles = function(count, size, originX, originY) {

      window.particles = window.particles || [];

      for (var i = 0; i <= count; i++) {
        var x = originX || Math.random() * window.innerWidth,
          y = originY || Math.random() * window.innerHeight;
        (function(particle) {
          window.particles.push(particle);
        })(new Particle(x, y, size));
      }
    };

    /* ======================= */

    resize();

    window.addEventListener('resize', resize, false);

    generateParticles(settings.count, settings.size, settings.startOrigin.x, settings.startOrigin.y);

    animate();

    /* ======================= */

    function resize() {
      width = window.innerWidth;
      height = window.innerHeight;

      if (window.particles) {

        for (var i = 0; i < window.particles.length; i++) {
          if (window.particles[i].position.x > width) {
            window.particles[i].stop();
            window.particles[i].position.x = width;
          }

          if (window.particles[i].position.y > height) {
            window.particles[i].stop();
            window.particles[i].position.y = height;
          }

        }
      }

    }

    function renderCanvas() {

      ctx.globalCompositeOperation = 'destination-out';
      ctx.fillStyle = 'rgba(150,0,0,0)';

      ctx.fillRect(0, 0, width, height);

      ctx.globalCompositeOperation = 'source-over';
      ctx.fillStyle = "rgba(255,255,255,1)";

      if (window.particles) {
        for (var i = 0; i < window.particles.length; i++) {
          var ball = window.particles[i];
          ctx.beginPath();
          ctx.arc(ball.position.x, ball.position.y, ball.radius, 0, Math.PI * 2, false);
          ctx.closePath();
          ctx.fill();
        }
      }

    }

    function animate(time) {
      requestAnimationFrame(animate);

      if (width !== canvas.width) {
        canvas.width = width;
      }

      if (height !== canvas.height) {
        canvas.height = height;
      }

      if (window.particles) {
        for (var i = 0; i < window.particles.length; i++) {
          var ball = window.particles[i];
          if (!ball.getPosition(time)) {
            var x = Math.random() * width,
              y = Math.random() * height,
              speed = Math.random() * (settings.maxSpeed / 2) + settings.minSpeed;
            ball.move(x, y, speed);
          }
        }
      }

      renderCanvas();
    }

  }, {
    "./particles": "c:\\work\\particles-animation\\src\\js\\particles\\index.js"
  }],
  "c:\\work\\particles-animation\\src\\js\\particles\\index.js": [function(require, module, exports) {
    var Particle;

    Particle = function(posx, posy, radius) {

      if (radius < 0) {
        throw "Ошибка! Дан радиус частицы " + radius + " пикселей, но радиус не может быть отрицательным!";
      }

      this.position = {
        x: posx || 0,
        y: posy || 0
      };

      if (typeof radius == 'function') {
        this.radius = radius();
      } else {
        this.radius = radius || 0;
      }

      this.status = 'standing'; // Статусы: standing || moving

      this.direction = this.position;

      this.speed = 1; // 1 пиксель в секунду

      this.spotlightTimeStamp = undefined;

    };

    Particle.prototype.stop = function() {
      this.status = 'standing';
      this.spotlightTimeStamp = undefined;
      this.direction = this.position;
    };

    Particle.prototype.move = function(posx, posy, speed) {

      this.status = 'moving';

      this.spotlightTimeStamp = undefined;

      var deltaX = posx - this.position.x,
        deltaY = posy - this.position.y,
        distance = Math.sqrt(deltaX * deltaX + deltaY * deltaY);

      this.direction = {
        x: posx,
        y: posy,
        distance: distance,
        sin: deltaY / distance,
        cos: deltaX / distance
      };

      this.startPoint = this.position;

      this.speed = speed || 1;

    };

    Particle.prototype.getPosition = function getPosition(movetime) {

      var time = movetime / 1000;

      if (this.status == 'moving') {
        if (this.spotlightTimeStamp) {
          var deltaTime = time - this.spotlightTimeStamp,
            distance = (deltaTime * this.speed);

          var posy = this.direction.sin * distance,
            posx = this.direction.cos * distance;

          this.position = {
            x: posx + this.startPoint.x,
            y: posy + this.startPoint.y
          };

          if (distance > this.direction.distance) {
            this.status = 'standing';
            this.spotlightTimeStamp = undefined;
            this.position = this.direction;
          }

        } else {
          this.spotlightTimeStamp = time;
        }
        return this.position;
      } else {
        return false;
      }
    };

    module.exports = Particle;
  }, {}]
}, {}, ["./src/js/index.js"])


'use strict';

;( function ( document, window, index )
{
    var inputs = document.querySelectorAll( '.inputfile' );
    Array.prototype.forEach.call( inputs, function( input )
    {
        var label     = input.nextElementSibling,
            labelVal = label.innerHTML;

        input.addEventListener( 'change', function( e )
        {
            var fileName = '';
            if( this.files && this.files.length > 1 )
                fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
            else
                fileName = e.target.value.split( '\\' ).pop();

            if( fileName )
                label.querySelector( 'span' ).innerHTML = fileName;
            else
                label.innerHTML = labelVal;
        });

        // Firefox bug fix
        input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
        input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
    });
}( document, window, 0 ));
   </script>
 </body>
 </html>
